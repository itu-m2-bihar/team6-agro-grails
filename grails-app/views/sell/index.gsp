<%--
  Created by IntelliJ IDEA.
  User: HP
  Date: 28/08/2022
  Time: 09:07
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>${controllerName}</title>
    <g:javascript library="jquery" />
</head>

<body>
<div class="container">
    <div class="py-5 text-center">
        <h2>Vendre un produit Agricole</h2>
        <p class="lead">Renseignez les informations sur le produit <strong>Agricole</strong> que vous vendez !</p>
    </div>

    <div class="row">
        <div class="offset-md-2 col-md-8 c-card">
            <h4 class="mb-3">Quel produit ?</h4>
            <g:form class="needs-validation" novalidate="novalidate" url="[controller: 'sell', action: 'makeASell']">
                <div class="row">
                    <div class="col-md-6 mb-3">
                        <label for="category">Catégorie</label>
                        <g:select class="custom-select d-block w-100" name="category" from="${categories}" optionKey="id" optionValue="name"
                                  noSelection="['':'Choisir une catégorie']"
                                  onchange="updateProduct('${g.createLink( controller:'sell', action:'findProductByCategory')}', 'product-container')"
                                  />
                        <div class="invalid-feedback">
                            Veuillez selectionner une catégorie
                        </div>
                    </div>
                    <div id="product-container" class="col-md-6 mb-3">
                        <label for="product">Le produit</label>
                        <select id="product" name="product" class="custom-select d-block w-100" optionKey="id" optionValue="name" from="${products}" required="required" ></select>
                        <div class="invalid-feedback">
                            Veuillez selectionner un produit
                        </div>
                    </div>

                </div>
                <p class="small">Si le produit que vous voulez vendre n'apparaît pas, veuillez cliquer <a href="">ici</a></p>
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="mb-3">La quantité ( Kg )</h4>
                        <input onchange="totalPrice()" type="number" class="form-control" id="quantity" name="quantity" placeholder="ex : 5">
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-9">
                        <h4 class="mb-3">Le Prix ( Ariary )</h4>
                        <input onchange="totalPrice()" type="number" class="form-control" id="price" name="price" placeholder="prix par Kg">
                    </div>
                    <div class="col-md-3">
                        <h4 class="mb-3">Total</h4>
                        <input type="text" class="form-control" id="total" disabled>
                    </div>
                </div>
                <br class="mb-4">
                <button class="btn btn-primary btn-lg btn-block" type="submit">Publier le produit</button>
            </g:form>
        </div>
    </div>
</div>
<script type="text/javascript">
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');

            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
    function totalPrice(){
        let unite = document.getElementById("price").value;
        let quantity = document.getElementById("quantity").value;
        document.getElementById("total").value = unite * quantity + " Ar";
    }
</script>
</body>
</html>