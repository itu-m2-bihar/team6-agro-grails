<%--
  Created by IntelliJ IDEA.
  User: HP
  Date: 28/08/2022
  Time: 09:07
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>${controllerName}</title>
</head>

<body>
<div class="container">

    <table class="table table-bordered">
        <thead>
        <tr>
            <th style="text-align: center" scope="col">#</th>
            <th style="text-align: center" scope="col">Produit</th>
            <th style="text-align: center" scope="col">Quantity</th>
            <th style="text-align: center" scope="col">Date</th>
        </tr>
        </thead>
        <tbody>
        <g:each var="request" in="${requests}">
            <tr>
                <th style="text-align: right" scope="row">${request.id}</th>
                <td>${request.product.name}</td>
                <td align="right"><g:formatNumber number="${request.quantity}" format="###,###"/></td>
                <td><g:formatDate format="dd/MM/yyyy" date="${request.dateCreated}"/></td>
                <td>
                    <a href="${createLink(controller: "myRequests", action: "remove", params: [id: request.id])}" class="btn btn-sm btn-secondary">Supprimer</a>
                    <a href="${createLink(controller: "myRequests", params: [id: request.id])}" class="btn btn-sm btn-dark">Modifier</a>
                </td>
                <td style="padding: 0">
                    <div style="display: flex;flex-flow: column">
                        <g:each var="proposal" in="${request.proposals}">
                            <div style="border: 1px solid #dee2e6; padding: 5px;display: flex;justify-content: space-between">
                                <div style="display: flex;align-items: center">
                                    ${proposal.proposer.username} vous a proposé<strong>MGA<g:formatNumber number="${proposal.price}" format="###,###"/></strong>
                                </div>
                                <g:if test="${proposal.proposalState == agro.ProposalState.CREATED.ordinal()}">
                                    <a href="${createLink(controller: "myRequests", action: "acceptProposal", params: [requestId: request.id, proposalId: proposal.id])}" class="btn btn-dark">Accepter</a>
                                </g:if>
                                <g:elseif test="${proposal.proposalState == agro.ProposalState.PAID.ordinal()}">
                                    <button disabled class="btn btn-success">Payé</button>
                                </g:elseif>
                                <g:elseif test="${proposal.proposalState == agro.ProposalState.CLOSED.ordinal()}">
                                    <button disabled class="btn btn-secondary">Fermé</button>
                                </g:elseif>
                                <g:else>
                                    <button disabled class="btn btn-secondary">Désactivé</button>
                                </g:else>
                            </div>
                        </g:each>
                    </div>

                </td>
            </tr>

        </g:each>
        </tbody>
    </table>
</div>
</body>
</html>