<%--
  Created by IntelliJ IDEA.
  User: HP
  Date: 28/08/2022
  Time: 09:07
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>${controllerName}</title>
</head>

<body>
<div class="container">

    <table class="table table-bordered">
        <thead>
        <tr>
            <th style="text-align: center" scope="col">#</th>
            <th style="text-align: center" scope="col">Produit</th>
            <th style="text-align: center" scope="col">Prix unitaire</th>
            <th style="text-align: center" scope="col">Quantité</th>
            <th style="text-align: center" scope="col">Total</th>
            <th style="text-align: center" scope="col">Date</th>
            <th style="text-align: center" scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        <g:each var="sale" in="${sales}">
            <tr>
                <th style="text-align: right" scope="row">${sale.id}</th>
                <td>${sale.product.name}</td>
                <td align="right"><g:formatNumber number="${sale.price}" format="###,###"/> Ar</td>
                <td align="right"><g:formatNumber number="${sale.quantity}" format="###,###"/></td>
                <td align="right"><g:formatNumber number="${sale.quantity * sale.price}" format="###,###"/> Ar</td>
                <td><g:formatDate format="dd/MM/yyyy" date="${sale.dateCreated}"/></td>
                <td>
                    <g:if test="${sale.saleState >= 2}">
                        <button disabled class="btn btn-sm btn-success">Vendu</button>
                    </g:if>
                    <g:else>
                        <a href="${createLink(controller: "OnSale", action: "remove", params: [id: sale.id])}" class="btn btn-sm btn-secondary">Supprimer</a>
                    </g:else>
                </td>
            </tr>
        </g:each>
        </tbody>
    </table>
</div>
</body>
</html>