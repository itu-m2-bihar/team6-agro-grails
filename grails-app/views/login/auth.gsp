<g:set var='securityConfig' value='${applicationContext.springSecurityService.securityConfig}'/>
<html>
<head>
    <meta name="layout" content="signin"/>
    <s2ui:title messageCode='spring.security.ui.login.title'/>
    %{--	<asset:stylesheet src='spring-security-ui-auth.css'/>--}%
    <asset:stylesheet src='signin.css'/>
</head>

<body>
%{--<p/>--}%
%{--<div class="login s2ui_center ui-corner-all" style='text-align:center;'>--}%
%{--	<div class="login-inner">--}%
%{--		<s2ui:form type='login' focus='username'>--}%
%{--			<div class="sign-in">--}%
%{--				<h2><g:message code='spring.security.ui.login.signin'/></h2>--}%
%{--				<table>--}%
%{--					<tr>--}%
%{--						<td><label for="username"><g:message code='spring.security.ui.login.username'/></label></td>--}%
%{--						<td><input type="text" name="${securityConfig.apf.usernameParameter}" id="username" class='formLogin' size="20"/></td>--}%
%{--					</tr>--}%
%{--					<tr>--}%
%{--						<td><label for="password"><g:message code='spring.security.ui.login.password'/></label></td>--}%
%{--						<td><input type="password" name="${securityConfig.apf.passwordParameter}" id="password" class="formLogin" size="20"/></td>--}%
%{--					</tr>--}%
%{--					<tr>--}%
%{--						<td colspan='2'>--}%
%{--							<input type="checkbox" class="checkbox" name="${securityConfig.rememberMe.parameter}" id="remember_me" checked="checked"/>--}%
%{--							<label for='remember_me'><g:message code='spring.security.ui.login.rememberme'/></label> |--}%
%{--							<span class="forgot-link">--}%
%{--								<g:link controller='register' action='forgotPassword'><g:message code='spring.security.ui.login.forgotPassword'/></g:link>--}%
%{--							</span>--}%
%{--						</td>--}%
%{--					</tr>--}%
%{--					<tr>--}%
%{--						<td colspan='2'>--}%
%{--							<s2ui:linkButton elementId='register' controller='register' messageCode='spring.security.ui.login.register'/>--}%
%{--							<s2ui:submitButton elementId='loginButton' messageCode='spring.security.ui.login.login'/>--}%
%{--						</td>--}%
%{--					</tr>--}%
%{--				</table>--}%
%{--			</div>--}%
%{--		</s2ui:form>--}%
%{--	</div>--}%
%{--</div>--}%
<div class="container">
    <s2ui:form type="login" focus="username" class="form-signin">
        <asset:image src="day33-pumpkin.png" width="200" height="200"/>
        <h1 class="h3 mb-3 font-weight-normal">Connexion</h1>
        <input name="username" type="text" id="username" class="form-control" placeholder="Nom utilisateur" required
               autofocus>
        <input name="password" type="password" id="password" class="form-control" placeholder="Mot de passe"
               required>

        <div class="checkbox mb-3">
            <label>
                <input type="checkbox" class="checkbox" name="${securityConfig.rememberMe.parameter}" id="remember_me"
                       checked="checked"/> Remember me
            </label>
        </div>
        <button id="loginButton_submit" class="btn btn-lg btn-secondary btn-block" type="submit">Se connecter</button>
        ou
        <a href="${createLink(uri: '/register')}" class="btn btn-lg btn-dark btn-block">S'inscrire</a>
    </s2ui:form>
    <g:if test="${flash.success}">
        <div class="col-5 mx-auto alert alert-success">
            <strong>Succès!</strong> ${flash.success}.
        </div>
    </g:if>
    <g:if test="${flash.message}">
        <div class="col-5 mx-auto alert alert-danger">
            <strong>Erreur!</strong> ${flash.message}.
        </div>
    </g:if>
</div>
</body>
</html>
