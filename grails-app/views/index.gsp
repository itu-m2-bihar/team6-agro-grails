<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Welcome to Grails</title>
</head>
<body>
    <div class="container">
        <div class="row mt-5">
            <h1 class="mx-auto text-center" style="width: 500px;">
                Produits du jour
            </h1>
        </div>

        <div class="row d-flex justify-content-center">
            <img class="rounded" width="200" height="200" src="https://images.pexels.com/photos/7194784/pexels-photo-7194784.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1">
            <img class="rounded" width="200" height="200"  src="https://images.pexels.com/photos/1093038/pexels-photo-1093038.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500">
            <img class="rounded" width="200" height="200" src="https://images.pexels.com/photos/533280/pexels-photo-533280.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1">
            <img class="rounded" width="200" height="200" src="https://images.pexels.com/photos/1343537/pexels-photo-1343537.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1">
            <img class="rounded" width="200" height="200"  src=https://images.pexels.com/photos/4113808/pexels-photo-4113808.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500">
        </div>
    </div>


</body>
</html>
