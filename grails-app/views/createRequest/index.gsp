<%--
  Created by IntelliJ IDEA.
  User: randriamanjakamariano
  Date: 28/08/2022
  Time: 12:07
--%>


<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>${controllerName}</title>
    <g:javascript library="jquery" />
</head>

<body>
<div class="container">
    <div class="py-5 text-center">
        <h2>Vous Recherchez un Produit ?</h2>
        <p class="lead">Renseignez les informations sur le produit Agricole que vous recherchez !</p>
    </div>

    <div class="row">
        <div class="offset-md-2 col-md-8 c-card">
            <h4 class="mb-3">Quel produit ?</h4>
%{--<g:form class="form-signin" name="myForm" url="[controller: 'register']">--}%
            <g:form class="needs-validation" name="myForm" url="[controller: 'createRequest', action: 'publy']">
                <div class="row">
                    <div class="col-md-6 mb-3">
                        <label for="category">Catégorie</label>
                        <g:select class="custom-select d-block w-100" name="category" from="${categories}" optionKey="id" optionValue="name"
                                  noSelection="['':'Choisir une catégorie']"
                                  onchange="updateProduct('${g.createLink( controller:'sell', action:'findProductByCategory')}', 'product-container')"
                        />
                        <div class="invalid-feedback">
                            Veuillez selectionner une catégorie
                        </div>
                    </div>
                    <div id="product-container" class="col-md-6 mb-3">
                        <label for="product">Le produit</label>
                        <select id="product" name="product" class="custom-select d-block w-100" optionKey="id" optionValue="name" from="${products}" required="required" ></select>
                        <div class="invalid-feedback">
                            Veuillez selectionner un produit
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="mb-3">La quantité (Kg)</h4>
                        <input name="quantity" type="number" class="form-control" id="quantity" placeholder="ex : 12">
                    </div>
                </div>
                <br class="mb-4">
                <button class="btn btn-primary btn-lg btn-block" type="submit">Publier la demande</button>
                <g:renderErrors bean="${request}" />
            </g:form>
        </div>
    </div>
</div>
<script>
    (function() {
        'use strict';

        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');

            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
</script>
</body>
</html>