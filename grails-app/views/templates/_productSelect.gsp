<label for="product">Le produit</label>
<g:select name="product" class="custom-select d-block w-100" optionKey="id" optionValue="name" from="${product}" required="required" />
<div class="invalid-feedback">
    Veuillez selectionner un produit
</div>