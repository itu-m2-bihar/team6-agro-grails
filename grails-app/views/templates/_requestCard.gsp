<g:set var="proposal" value="${curUser ? req.findProposal(curUser.proposals) : null}"/>
<form
    <g:if test="${proposal == null}">
        onsubmit="propose(event, ${req.id}, this, '${g.createLink(controller: "request", action: "propose")}')"
    </g:if>
    <g:else>
        onsubmit="cancelProposal(event, ${proposal.id}, ${req.id}, this, '${g.createLink(controller: "request", action: "cancelProposal")}')"
    </g:else>>
    <div class="card mb-4 shadow-sm">
        <div class="user">
            <div class="user-image-container">
                ${req.user.username[0]}
            </div>
            <span>${req.user.username}</span>
        </div>

        <div class="img-container">
            <img class="card-img-top"
                 src="${resource(dir: "product-image", file: "/${req.product.id}-${req.product.image}")}"
                 alt="Card image cap">
        </div>

        <div class="card-body">
            <p class="title">${req.product.name}</p>
            <small class="card-text text-muted">Je cherche 5 kg de pomme</small>
            <hr/>

            <div class=" mt-3 d-flex justify-content-between align-items-center">
                <span>Quantité</span>
                <span class=""><g:formatNumber number="${req.quantity}" format="###,###"/> Kg</span>
            </div>
        </div>

        <div class="card-footer d-flex" style="flex-direction: column-reverse;position:relative;">
            <g:if test="${req.requestState == agro.RequestState.DEACTIVATED.ordinal()}">
                <button disabled class="btn btn-primary btn-block">Désactivé</button>
            </g:if>
            <g:elseif test="${req.requestState >= agro.RequestState.CLOSED.ordinal()}">
                <button disabled class="btn btn-grey btn-block">Fermé</button>
            </g:elseif>
            <g:else>
                <sec:ifNotLoggedIn>
                    <a class="btn btn-primary btn-block"
                       href="${createLink(controller: "login")}">Faire une proposition</a>
                </sec:ifNotLoggedIn>
                <sec:ifLoggedIn>
                    <g:if test="${proposal != null}">
                        <button type="submit" class="btn btn-secondary"
                                onclick="cancelProposal(${proposal.id})">Annuler</button>

                        <div class="d-flex align-items-center mb-1" >
                            <label style="width: 60%;" for="price" class="m-0">MGA (Prix total) </label>
                            <input disabled value="${proposal.price}" name="price"
                                   type="number"
                                   class="form-control" id="price">
                        </div>
                    </g:if>
                    <g:else>
                        <button type="submit" class="btn btn-primary floating-btn">Proposer</button>

                        <div style="gap:5px;margin-bottom:10px;" class="d-flex align-items-center">
                            <label style="width: 60%;" for="price" class="m-0">MGA (Prix total) </label>
                            <input name="price" type="number" class="form-control" id="price">
                        </div>
                    </g:else>
                </sec:ifLoggedIn>
            </g:else>

        </div>
    </div>
</form>
