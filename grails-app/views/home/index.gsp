<%--
  Created by IntelliJ IDEA.
  User: HP
  Date: 28/08/2022
  Time: 09:07
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <asset:stylesheet src="home.css"/>
    <title>Accueil </title>
</head>

<body>
%{--to load spring security on the page copy this line (18) --}%
    <g:set var="spring" bean="springSecurityService"/>
%{--    to display current user id use : ${spring.currentUser.id}--}%
    <div class="container">
        <div class="row pt-5">
            <div class="left-product-list col-lg-3 p-0">
                <div class="head btn w-100 not-rounded text-uppercase bold-text">Produits disponibles</div>
                <g:form controller="buy">
                    <ul class="products-list p-0">
                    <g:each var="product" in="${products}">
                        <li class="product-item">
                            <input onchange="resetPage()" type="checkbox" name="produits[]"
                                   value="${product.string}" ${product.checked ? "checked" : ''}>
                            <label>${product.string}</label>
                        </li>
                    </g:each>
                        <li><br/></li>
                    </ul>
                    <input id="page" type="hidden" name="page" value="${page as Integer ?: 1}">
                    <input type="hidden" name="max" value="10">
                    <div class="scrollIndicator"> </div>
                    <g:submitButton class="btn green-btn not-rounded" name="submit" value="FILTRER"/>
                </g:form>
            </div>
            <div class="col-lg-9">
                <div class="vegetables">
                    <div class="text">
                        <p class="bold-text green-text">FRUIT FRESH</p>
                        <span class="bold-text big-text">Vegetable<br/>100% Organic</span>
                        <p class="light-text">free pickup and Delivery Available</p>
                        <a class="btn green-btn" href="${createLink(uri: '/buy')}">Voir les produits en vente</a>
                    </div>
                </div>
            </div>
        </div>
        <div id="product-recommendation" class="row recommended text-center">
            <h3 class="big-text bold-text w-100 mt-4 mb-4">Les personnes qui ont acheté les mêmes produits que vous on aussi acheté</h3>
        </div>
        <div class="row banner mt-5">
            <div class="col-6">
                <div class="banner__pic">
                    <img class="w-100" src="https://preview.colorlib.com/theme/ogani/img/banner/xbanner-1.jpg.pagespeed.ic.aDN3QrExt6.webp" alt="" >
                </div>
            </div>
            <div class="col-6">
                <div class="banner__pic">
                    <img class="w-100" src="https://preview.colorlib.com/theme/ogani/img/banner/xbanner-2.jpg.pagespeed.ic.-2eeuFVLcY.webp" alt="" >
                </div>
            </div>
        </div>
        <br/>
        <br/>
        <br/>
        <br/>
    <g:if test="${spring.currentUser}">
        <div id="userId" data-id="${spring.currentUser.id}">
    </g:if>
</div>
<script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
<g:javascript type="application/javascript">
    $(document).ready(function() {
        let userId = $("#userId").data('id');
        let paramsVal = {
            state:0
        }
        if (userId) {
            paramsVal.state = 1;
            paramsVal.userId = userId
        }
        console.log("here", paramsVal)
        let baseUrl = "https://seed-py.herokuapp.com"
        // let baseUrl = "http://127.0.0.1:5000"
        $.ajax({
            method: 'GET',
            data: paramsVal,
            url: baseUrl + "/recommendation/recommend/product",
            success: function(res){
                for(const [i, product] of res.data.entries()) {
                    console.log(product)
                    if(i > 3) break
                    window.grailsSupport = {
                        assetsRoot : '${resource(dir: "product-image", file: "/")}'
                    };
                    const image = window.grailsSupport.assetsRoot + product['product_id'] + '-' + product['image']
                    const productHtml = `
                        <div class="col-lg-3 col-6 mt-2">
                            <div class="recommend">
                                <div class="img-container">
                                    <img class="card-img-top"
                                         src="` + image + `"
                                         alt=` + product.image +`>
                                </div>
                                <span class="bold-text">` + product['product_name'] + `</span>
                            </div>
                        </div>
                    `
                    $("#product-recommendation").append(productHtml)
                }
            },error: function(request, error) {
                console.log("error", JSON.stringify(request))
            }
        })
    })
</g:javascript>
</body>
</html>