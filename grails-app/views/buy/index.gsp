<%--
  Created by IntelliJ IDEA.
  User: HP
  Date: 28/08/2022
  Time: 09:07
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>${controllerName}</title>
</head>

<body>
<div class="container">
    <div class="row mt-3">
        <div class="left-product-list col-lg-3 p-0">
            <div class="head btn w-100 not-rounded text-uppercase bold-text">Produits disponibles</div>
            <g:form controller="buy">
                <ul class="products-list buy p-0">
                    <g:each var="product" in="${products}">
                        <li class="product-item">
                            <input onchange="resetPage()" type="checkbox" name="produits[]"
                                   value="${product.string}" ${product.checked ? "checked" : ''}>
                            <label>${product.string}</label>
                        </li>
                    </g:each>
                    <li><br/></li>
                    <li><br/></li>
                    <li><br/></li>
                </ul>
                <input id="page" type="hidden" name="page" value="${page as Integer ?: 1}">
                <input type="hidden" name="max" value="10">

                <div class="scrollIndicator"></div>
                <g:submitButton class="btn green-btn not-rounded" name="submit" value="FILTRER"/>
            </g:form>
        </div>

        <div class="col-lg-9">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th style="text-align: left" scope="col">Vendeur</th>
                    <th style="text-align: center" scope="col">Produit</th>
                    <th style="text-align: center" scope="col">Prix/Kg(Ar)</th>
                    <th style="text-align: center" scope="col">Quantité</th>
                    <th style="text-align: center" scope="col">Total(Ar)</th>
                    <th style="text-align: center" scope="col">Date</th>
                    <th style="text-align: center" scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                <g:each var="sale" in="${sales}">
                    <tr>
                        <th style="text-align: right" scope="row">${sale.user.username}</th>
                        <td>${sale.product.name}</td>
                        <td align="right"><g:formatNumber number="${sale.price}" format="###,###"/></td>
                        <td align="right"><g:formatNumber number="${sale.quantity}" /></td>
                        <td align="right"><g:formatNumber number="${sale.quantity * sale.price}" format="###,###"/></td>
                        <td><g:formatDate format="dd/MM/yyyy" date="${sale.dateCreated}"/></td>
                        <td>
                            <button style="${cart == null || !(cart as List<String>).contains(sale.id as String) ? "" : "display: none"}"
                                    id="add-to-cart${sale.id}"
                                    onclick="addToCart(${sale.id}, 'add-to-cart${sale.id}', 'remove-from-cart${sale.id}')"
                                    type="button" class="btn btn-primary btn-sm">Ajouter</button>
                            <button style="${cart == null || !(cart as List<String>).contains(sale.id as String) ? "display: none" : ""}"
                                    onclick="removeFromCart(${sale.id}, 'add-to-cart${sale.id}', 'remove-from-cart${sale.id}')"
                                    id="remove-from-cart${sale.id}" type="button"
                                    class="btn btn-secondary btn-sm">Retirer</button>
                        </td>
                    </tr>
                </g:each>
                </tbody>
            </table>
            <g:if test="${page != null}">
                <nav aria-label="...">
                    <ul class="pagination">
                        <li class="page-item ${page == 1 ? 'active' : ''}"><a
                                onclick="changePage(${page == 1 ? 1 : page-1})"
                                class="page-link"
                                href="#">${page == 1 ? 1 : page - 1}</a></li>
                        <li class="page-item ${page != 1 ? 'active' : ''}">
                            <a onclick="changePage(${page == 1 ? 2 : page})" href="#" class="page-link">
                                ${page == 1 ? 2 : page}
                            </a>
                        </li>
                        <li class="page-item"><a onclick="changePage(${page == 1 ? 3 : page+1})" class="page-link"
                                                 href="#">${page == 1 ? 3 : page + 1}</a></li>
                    </ul>
                </nav>

                <g:javascript>
                    function changePage(page) {
                        $("#page").val(page);
                        $("#submit").click();
                    }
                </g:javascript>
            </g:if>
        </div>
    </div>





    <g:javascript>
        function resetPage() {
            $("#page").val(1);
        }
    </g:javascript>
</div>
</body>
</html>