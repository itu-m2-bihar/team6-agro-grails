<%--
  Created by IntelliJ IDEA.
  User: HP
  Date: 28/08/2022
  Time: 09:07
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>${controllerName}</title>
</head>

<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-5 d-none d-md-block sidebar">
            <h4 class="mb-3">Prédire le prix d'un produit sur 6 mois</h4>
            <form id="predict-form" class="needs-validation" novalidate>
                <div class="row">
                    <div class="col-md-6 mb-3">
                        <label for="category">Catégorie</label>
                        <g:select class="custom-select d-block w-100" name="category" from="${categories}" optionKey="id" optionValue="name"
                                  noSelection="['':'Choisir une catégorie']"
                                  onchange="updateProduct()"
                        />
                        <div class="invalid-feedback">
                            Veuillez selectionner une catégorie
                        </div>
                    </div>
                    <div id="product-container" class="col-md-6 mb-3">
                        <label>Le produit</label>
                        <select name="product" class="custom-select d-block w-100" optionKey="id" optionValue="name" from="${products}" required="required" ></select>
                        <div class="invalid-feedback">
                            Veuillez selectionner un produit
                        </div>
                    </div>
                </div>
                <br class="mb-4">
                <button class="btn btn-primary btn-lg btn-block" type="submit">Prédire</button>
            </form>

        </div>
        <div role="main" class="col-md-7 ml-sm-auto col-lg-7 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Prédire le prix d'un produit sur 6 mois</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group mr-2">
                        <button class="btn btn-sm btn-outline-secondary">Share</button>
                        <button class="btn btn-sm btn-outline-secondary">Export</button>
                    </div>
                </div>
            </div>

            <canvas class="my-4 w-100" id="predict-chart" width="900" height="380"></canvas>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="../../assets/js/vendor/popper.min.js"></script>
<script src="../../dist/js/bootstrap.min.js"></script>

<!-- Icons -->
<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
<script>
    feather.replace()
</script>

<!-- Graphs -->
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.7.1/dist/Chart.min.js"></script>
<script>
    $(document).ready(function() {
        let ctx = document.getElementById("predict-chart");
        let predictChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: ["Avril 2020", "Mai 2020", "Juin 2020", "Juillet 2020", "Août 2020", "Septembre 2020"],
                datasets: [{
                    data: [15339, 21345, 18483, 24003, 23489, 12034],
                    lineTension: 0,
                    backgroundColor: 'transparent',
                    borderColor: '#007bff',
                    borderWidth: 4,
                    pointBackgroundColor: '#007bff'
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: false
                        }
                    }]
                },
                legend: {
                    display: false,
                }
            }
        });

        $("#predict-form").on("submit", function (e) {
            e.preventDefault();
            let baseUrl = "https://seed-py.herokuapp.com"
            $.ajax({
                method: 'GET',
                data:{category: $("#category option:selected").text(), product: $("#product option:selected").text()},
                url:baseUrl + "/price/predict",
                success: function(res){
                    let chartData = [];
                    let chartLabels = res.data.map(predItem => {
                        chartData.push(predItem.pred)
                        return formatDate(predItem.month, predItem.year);
                    });
                    predictChart.data.labels = chartLabels;
                    predictChart.data.datasets[0].data = chartData;
                    predictChart.update();
                },error: function(request, error) {
                    console.log("error", JSON.stringify(request))
                }
            })
        });
    });

    function formatDate (month, year) {
        let monthStr = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
        return [monthStr[month-1],year].join(' ');
    }

    function updateProduct(){
        $.ajax({
            url:'${g.createLink( controller:'predict', action:'findProductByCategory')}',
            data: [category],
            type: 'get'
        }).done(
            function ( data ) {
                document.getElementById("product-container").innerHTML = data;
            }
        );
    }
</script>
</body>
</html>