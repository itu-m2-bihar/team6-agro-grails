<%--
  Created by IntelliJ IDEA.
  User: randriamanjakamariano
  Date: 28/08/2022
  Time: 13:37
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>${controllerName}</title>
</head>

<body>
<g:if test="${sales !=null && sales.size() != 0}">
    <div class="col-md-4 order-md-2 mb-4">
        <h4 class="d-flex justify-content-between align-items-center mb-3">
            <span class="text-muted">Your cart</span>
            <span class="badge badge-secondary badge-pill">${sales.size()}</span>
        </h4>
        <ul class="list-group mb-3">
            <g:each var="sale" in="${sales}">
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <div>
                        <h6 class="my-0">${sale.product.name}</h6>
                        <small class="text-muted">
                            le <g:formatDate format="dd/MM/yyyy" date="${sale.lastUpdated}"/>
                        </small>
                    </div>
                    <span class="text-muted">AR<g:formatNumber number="${sale.price * sale.quantity}" format="###,###"/></span>
                </li>
            </g:each>
        %{--        <li class="list-group-item d-flex justify-content-between bg-light">--}%
        %{--            <div class="text-success">--}%
        %{--                <h6 class="my-0">Promo code</h6>--}%
        %{--                <small>EXAMPLECODE</small>--}%
        %{--            </div>--}%
        %{--            <span class="text-success">-$5</span>--}%
        %{--        </li>--}%
            <li class="list-group-item d-flex justify-content-between">
                <span>Total (MGA)</span>
                <strong>AR<g:formatNumber number="${total}" format="###,###"/></strong>
            </li>
        </ul>

        <g:form controller="cart" action="confirmCart">
            <g:if test="${cart}">
                <input type="hidden" name="cartId" value="${cart.id}">
            </g:if>
            <sec:ifNotLoggedIn>
                <g:link controller="login" class="btn btn-secondary">Confirmer Achat</g:link>
            </sec:ifNotLoggedIn>
            <sec:ifLoggedIn>
                <button type="submit" class="btn btn-secondary">Confirmer Achat</button>
            </sec:ifLoggedIn>
        </g:form>


    </div>
</g:if>
<g:else>
    <div class="col-md-4 order-md-2 mb-4">
        Panier vide
    </div>
</g:else>

</body>
</html>