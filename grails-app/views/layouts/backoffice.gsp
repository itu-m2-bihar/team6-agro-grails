<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
    <g:layoutTitle default="Backoffice"/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
          integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;400;700&display=swap" rel="stylesheet">
    <asset:javascript src="application.js"/>
    <asset:stylesheet src="main.css"/>
    <g:javascript/>
    <g:layoutHead/>
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item ${actionName == 'addProduct' ? 'active' : ''}">
                <g:link class="nav-link" controller="backoffice" action="addProduct">Ajout de produit</g:link>
            </li>
            <li class="nav-item ${actionName == 'productStats' ? 'active' : ''}">
                <g:link class="nav-link" controller="backoffice" action="productStats">Stats produits</g:link>
            </li>
            <li class="nav-item ${actionName == 'userStats' ? 'active' : ''}">
                <g:link class="nav-link" controller="backoffice" action="userStats">Stats utilisateurs</g:link>
            </li>
            <li class="nav-item ${actionName == 'userModeration' ? 'active' : ''}">
                <g:link class="nav-link" controller="backoffice" action="userModeration">Modération Utilisateur</g:link>
            </li>
            <li class="nav-item ${actionName == 'generateScratchCard' ? 'active' : ''}">
                <g:link class="nav-link" controller="backoffice" action="generateScratchCard">Générer Carte à gratter</g:link>
            </li>
            <li class="nav-item">
                <g:link class="nav-link">Hello <sec:username/></g:link>
            </li>
            <li class="nav-item">
                <g:link controller="logout" class="nav-link">Logout</g:link>
            </li>
        </ul>
    </div>
</nav>


<g:layoutBody/>
<g:if test="${flash.success}">
    <div class="col-5 mx-auto alert alert-success">
        <strong>Succès!</strong> ${flash.success}.
    </div>
</g:if>
<g:if test="${flash.message}">
    <div class="col-5 mx-auto alert alert-danger">
        <strong>Erreur!</strong> ${flash.message}.
    </div>
</g:if>

<script src="https://code.jquery.com/jquery-3.6.1.min.js"
        integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js"
        integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+"
        crossorigin="anonymous"></script>

</body>
</html>
