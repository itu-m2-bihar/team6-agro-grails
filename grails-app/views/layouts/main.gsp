<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
    <g:layoutTitle default="Grails"/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
          integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;400;700&display=swap" rel="stylesheet">
    <asset:javascript src="application.js"/>
    <asset:stylesheet src="main.css"/>
    <g:javascript/>
    <g:layoutHead/>
</head>

<body>
<g:set var="spring" bean="springSecurityService"/>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a style="margin-right:18px" class="nav-brand" href="/"><asset:image src="logo.png" height="70"/></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item ${controllerName == 'home' ? 'active' : ''}">
                <a class="nav-link" href="/">Accueil<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item  ${controllerName == 'buy' ? 'active' : ''}">
                    <a class="nav-link" href="${createLink(uri: '/buy')}">Acheter</a>
            </li>
            <li class="nav-item  ${controllerName == 'request' ? 'active' : ''}">
                <a class="nav-link" href="${createLink(uri: '/request')}">Demandes</a>
            </li>
            <li class="nav-item ${controllerName == 'sell' ? 'active' : ''}">
                <a class="nav-link" href="${createLink(uri: '/sell')}">Vendre</a>
            </li>
            <li class="nav-item ${controllerName == 'createRequest' ? 'active' : ''}">
                <a class="nav-link" href="${createLink(uri: '/createRequest')}">Faire une demande</a>
            </li>
            <li class="nav-item ${controllerName == 'onSale' ? 'active' : ''}">
                <a class="nav-link" href="${createLink(uri: '/onSale')}">Mes produits en vente</a>
            </li>
            <li class="nav-item ${controllerName == 'myRequests' ? 'active' : ''}">
                <a class="nav-link" href="${createLink(uri: '/myRequests')}">Mes demandes</a>
            </li>
            <li class="nav-item ${controllerName == 'predict' ? 'active' : ''}">
                <a class="nav-link" href="${createLink(uri: '/predict')}">Prédiction</a>
            </li>
            <li class="nav-item ${controllerName == 'cart' ? 'active' : ''}">
                <a class="nav-link" href="${createLink(uri: '/cart')}">Panier</a>
            </li>
            <sec:ifLoggedIn>
                <li class="nav-item ${controllerName == 'buyed' ? 'active' : ''}">
                    <a class="nav-link" href="${createLink(uri: '/buyed')}">Historique d'achat</a>
                </li>
                <li class="nav-item">
                    <g:link class="nav-link">Hello <sec:username/></g:link>
                </li>
                <li class="nav-item">
                    <g:link controller="logout" class="nav-link">Logout</g:link>
                </li>
                <li class="nav-item">
                    <g:link controller="buyToken" class="nav-link">Token: <g:formatNumber number="${spring.currentUser.virtualMoney}" format="###,###"/></g:link>
                </li>

            </sec:ifLoggedIn>
        </ul>
    </div>
</nav>


<g:layoutBody/>
<g:if test="${flash.success}">
    <div class="col-5 mx-auto alert alert-success">
        <strong>Succès!</strong> ${flash.success}.
    </div>
</g:if>
<g:if test="${flash.message}">
    <div class="col-5 mx-auto alert alert-danger">
        <strong>Erreur!</strong> ${flash.message}.
    </div>
</g:if>
<footer class="footer">
    <div class="container">
        <span class="text-muted">Copyright 2022 | Made with ❤️️ by Mariano, Michael, Jourdan <br/></span>
    </div>
</footer>

<script src="https://code.jquery.com/jquery-3.6.1.min.js"
        integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js"
        integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+"
        crossorigin="anonymous"></script>

</body>
</html>
