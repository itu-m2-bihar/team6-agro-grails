<%--
  Created by IntelliJ IDEA.
  User: HP
  Date: 14/09/2022
  Time: 12:23
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>${controllerName}</title>
</head>

<body>
    <div class="container">
        <g:form class="col-md-4" method="POST">
            <label>Code</label>
            <input name="code" type="text" class="form-control">
            <button type="submit" class="btn btn-primary">OK</button>
        </g:form>
    </div>
</body>
</html>