<%--
  Created by IntelliJ IDEA.
  User: HP
  Date: 28/08/2022
  Time: 09:07
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>${controllerName}</title>
</head>

<body>
<main role="main">
    <section class="jumbotron text-center mb-0">
        <div class="container">
            <h1 class="jumbotron-heading">Les Demandes disponibles</h1>

            <p class="lead text-muted">Something short and leading about the collection below—its contents, the creator, etc. Make it short and sweet, but not too short so folks don't simply skip over it entirely.</p>

            <p>
                <a href="${createLink(controller: "myRequests")}" class="btn btn-secondary my-2">Voir mes demandes</a>
                <a href="${createLink(controller: "createRequest")}" class="btn btn-primary my-2">Faire une demande</a>
            </p>
        </div>
    </section>

    <div class="album py-5 bg-light">
        <div class="container">
            <div class="row">
                <g:each var="req" in="${requests}">
                    <div id="request-card-${req.id}" class="col-lg-4 col-md-6 col-sm-6 product">
                        <g:render template="/templates/requestCard" model="[req: req, curUser: curUser]" />
                    </div>
                </g:each>
            </div>
        </div>
    </div>

</main>
</body>
</html>