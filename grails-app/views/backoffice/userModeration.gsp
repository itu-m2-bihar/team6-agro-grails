<%--
  Created by IntelliJ IDEA.
  User: HP
  Date: 06/09/2022
  Time: 16:38
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
</head>

<body>
<div class="container">
    <div class="row mt-5">
        <h2 class="mx-auto text-center">
            Liste des utilisateurs
        </h2>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th style="text-align: center" scope="col">#</th>
                <th style="text-align: center" scope="col">Utilisateur</th>
                <th style="text-align: center" scope="col">Etat</th>
                <th>Rôles</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <g:each var="user" in="${users}">
                <tr>
                    <th>${user.id}</th>
                    <td style="text-align: right" scope="row">${user.username}</td>
                    <td style="text-align: right" scope="row">${user.accountLocked ? 'Banni' : 'Actif'}</td>
                    <td>
                        <g:each var="role" in="${user.getAuthorities()}">
                            ${role.authority},
                        </g:each>
                    </td>
                    <td>
                        <a href="${createLink(controller: "backoffice", action: "editUser", params: [id: user.id])}" class="btn btn-sm btn-primary">Modifier</a>
                    </td>
                </tr>
            </g:each>
            </tbody>
        </table>
        <nav aria-label="...">
            <ul class="pagination">
                <li class="page-item ${page == 1 ? 'disabled': ''}">
                    <a class="page-link" href="?page=${page-1}">Previous</a>
                </li>
                <li class="page-item active">
                    <a class="page-link" href="?page=${page}">${page}</a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="?page=${page+1}">${page+1}</a>
                </li>
                <li class="page-item">
                    <a class="page-link"href="?page=${page+2}">${page+2}</a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="?page=${page+1}">Next</a>
                </li>
            </ul>
        </nav>
    </div>
</div>
</body>
</html>