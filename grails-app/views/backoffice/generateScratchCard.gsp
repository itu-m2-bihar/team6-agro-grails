<%--
  Created by IntelliJ IDEA.
  User: HP
  Date: 14/09/2022
  Time: 10:01
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
</head>

<body>
<div class="container">
    <g:form class="col-md-4" action="generateScratchCard" method="POST">
        <label>Catégorie</label>
        <g:select name="categoryId" class="custom-select d-block w-100" optionKey="id" optionValue="amount"
                  from="${categories}" required="required"/>
        <button type="submit" class="btn btn-primary">Générer 20 modèles</button>
    </g:form>

    <div class="col-lg-9">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th style="text-align: center" scope="col">Code</th>
                <th style="text-align: center" scope="col">Valeur</th>
                <th style="text-align: center" scope="col">QR</th>
            </tr>
            </thead>
            <tbody>
            <g:each var="card" in="${cards}">
                <tr>
                    <td>${card.id}</td>
                    <td>${card.category.amount}</td>
                    <td><img src="https://api.qrserver.com/v1/create-qr-code/?size=200x200&data=${card.id}"/></td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>