<%--
  Created by IntelliJ IDEA.
  User: HP
  Date: 05/09/2022
  Time: 18:23
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
</head>

<body>
<div class="container">
<g:form class="col-md-4" action="save" enctype="multipart/form-data" method="POST">
        <label>Catégorie</label>
        <g:select name="categoryId" class="custom-select d-block w-100" optionKey="id" optionValue="name" from="${categories}" required="required" />
        <label>Nom</label>
        <input name="name" type="text" class="form-control">
        <div class="form-group">
            <g:field name="productImage" class="form-control" type="file" placeholder="Please Upload Image"/>
        </div>
        <button type="submit" class="btn btn-primary">Ajouter Produit</button>
    </div>
</g:form>

<div class="col-lg-9">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th style="text-align: center" scope="col">#</th>
            <th style="text-align: center" scope="col">Produit</th>
            <th style="text-align: center" scope="col">Category</th>
            <th style="text-align: center" scope="col">Image</th>
        </tr>
        </thead>
        <tbody>
        <g:each var="product" in="${products}">
            <tr>
                <th style="text-align: right" scope="row">${product.id}</th>
                <td>${product.name}</td>
                <td>${product.productCategory.name}</td>
                <td>
                    <g:if test="${product?.image}">
                        <img src="${resource(dir: "product-image", file: "/${product.id}-${product.image}")}" class="img-thumbnail" style="margin-top: 10px; height: 100px; width: 100px;"/>
                    </g:if>
                </td>
            </tr>
        </g:each>
        </tbody>
    </table>
</div>
</body>
</html>