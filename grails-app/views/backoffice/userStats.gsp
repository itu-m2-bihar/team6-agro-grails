<%--
  Created by IntelliJ IDEA.
  User: HP
  Date: 05/09/2022
  Time: 18:23
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
</head>

<body>
<div class="container">
    <div class="row mt-5">
        <h2 class="mx-auto text-center">
            Top 3 Utilisateurs avec le plus de ventes
        </h2>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th style="text-align: center" scope="col">Utilisateur</th>
                <th style="text-align: center" scope="col">Gains</th>
            </tr>
            </thead>
            <tbody>
            <g:each var="user" in="${top3EarningsUsers}">
                <tr>
                    <th style="text-align: right" scope="row">${user.usname}</th>
                    <td><g:formatNumber number="${user.total_earnings}" format="###,###"/></td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>
    <div class="row mt-5">
        <h2 class="mx-auto text-center">
            Top 3 Utilisateurs avec le plus d'achats
        </h2>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th style="text-align: center" scope="col">Utilisateur</th>
                <th style="text-align: center" scope="col">Montant Achat</th>
            </tr>
            </thead>
            <tbody>
            <g:each var="user" in="${top3PurchasesUsers}">
                <tr>
                    <th style="text-align: right" scope="row">${user.usname}</th>
                    <td><g:formatNumber number="${user.total_purchases}" format="###,###"/></td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>