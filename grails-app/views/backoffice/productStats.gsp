<%--
  Created by IntelliJ IDEA.
  User: HP
  Date: 05/09/2022
  Time: 18:23
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
</head>

<body>
<div class="container">

    <div class="row mt-5">
        <h2 class="mx-auto text-center">
            Top 3 Produits les plus demandés
        </h2>
    </div>
    <div class="row d-flex justify-content-center">
        <g:each var="product" in="${top3RequestedProducts}">
            <div style="display: flex;align-items: center;flex-flow: column">
                <img class="rounder" src="${resource(dir: "product-image", file: "/${product.pid}-${product.image}")}" class="img-thumbnail" style="height: 200px; width: 200px; border: 1px solid black"/>
                <div>${product.pname}: <g:formatNumber number="${product.request_popularity}" format="###,###"/>pts</div>
            </div>
        </g:each>
    </div>

    <div class="row mt-5">
        <h2 class="mx-auto text-center">
            Top 3 Produits les plus vendus
        </h2>
    </div>
    <div class="row d-flex justify-content-center">
        <g:each var="product" in="${top3SoldProducts}">
            <div style="display: flex;align-items: center;flex-flow: column">
                <img class="rounder" src="${resource(dir: "product-image", file: "/${product.pid}-${product.image}")}" class="img-thumbnail" style="height: 200px; width: 200px; border: 1px solid black"/>
                <div>${product.pname}: <g:formatNumber number="${product.sale_popularity}" format="###,###"/>pts</div>
            </div>
        </g:each>
    </div>

    <div class="row mt-5">
        <h2 class="mx-auto text-center">
            Top 3 Produits les plus populaires (demandes + ventes)
        </h2>
    </div>
    <div class="row d-flex justify-content-center">
        <g:each var="product" in="${top3PopularProducts}">
            <div style="display: flex;align-items: center;flex-flow: column">
                <img class="rounder" src="${resource(dir: "product-image", file: "/${product.pid}-${product.image}")}" class="img-thumbnail" style="height: 200px; width: 200px; border: 1px solid black"/>
                <div>${product.pname}: <g:formatNumber number="${product.popularity}" format="###,###"/>pts</div>
            </div>
        </g:each>
    </div>
</div>
</body>
</html>