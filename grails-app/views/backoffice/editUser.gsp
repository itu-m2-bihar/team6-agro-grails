<%--
  Created by IntelliJ IDEA.
  User: micha
  Date: 9/12/2022
  Time: 3:55 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
</head>

<body>
<div class="container">
<g:form class="col-md-4" action="saveEdit" methRod="POST">
    <label>Nom d'utilisateur</label>
    <input disabled name="username" value="${user.username}" type="text" class="form-control">
    <label>Roles</label>
    <ul>
        <g:each var="role" in="${roles}">
            <li>
                <input type="checkbox" name="roles[]"
                       value="${role.id}" ${user.getAuthorities().contains(role) ? "checked" : ''}>
                <label>${role.authority}</label>
            </li>
        </g:each>
    </ul>
    <input type="hidden" name="userId" value="${user.id}">
    <button type="submit" name="submit" value="ban" class="btn btn-danger">Bannir</button>
    <button type="submit" name="submit" value="save" class="btn btn-primary">Sauvegarder</button>
    </div>
</g:form>
</div>
</body>
</html>