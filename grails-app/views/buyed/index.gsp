<%--
  Created by IntelliJ IDEA.
  User: HP
  Date: 28/08/2022
  Time: 09:07
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>${controllerName}</title>
</head>

<body>
<div class="container">
    <div class="col-md-4 order-md-2 mb-4">
    <h1>Historique d'achats</h1>
        <br/>
    <g:each var="cart" in="${panier}">
        <h4 class="d-flex justify-content-between align-items-center mb-3">
            <span class="text-muted">Date validation : <g:formatDate format="dd/MM/yyyy" date="${cart.dateCreated}"/></span>
            <span class="badge badge-secondary badge-pill">${cart.sales.size()}</span>
        </h4>
        <ul class="list-group mb-3">
            <g:each var="sale" in="${cart.sales}">
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <div>
                        <h6 class="my-0">${sale.product.name}</h6>
                        <small class="text-muted">
                            Vendeur : ${sale.user.username}
                        </small>
                    </div>
                    <span class="text-muted">AR <g:formatNumber number="${sale.price * sale.quantity}" format="###,###"/></span>
                </li>
            </g:each>
%{--            <li class="list-group-item d-flex justify-content-between">--}%
%{--                <span>Total (MGA)</span>--}%
%{--                <strong>AR<g:formatNumber number="${total}" format="###,###"/></strong>--}%
%{--            </li>--}%
        </ul>
    </g:each>
    </div>
</div>
</body>
</html>