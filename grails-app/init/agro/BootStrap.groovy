package agro

class BootStrap {

    def init = { servletContext ->
        def adminRole = Role.findOrSaveWhere(authority: 'ROLE_ADMIN')
        Role.findOrSaveWhere(authority: 'ROLE_USER')
        User superAdmin = new User(username: "Superadmin", password: "admin").save()
        UserRole.create(superAdmin, adminRole)
        UserRole.withSession {
            it.flush()
            it.clear()
        }

        ScratchCardCategory.findOrSaveWhere(amount: 1000.0d)
        ScratchCardCategory.findOrSaveWhere(amount: 5000.0d)
        ScratchCardCategory.findOrSaveWhere(amount: 10000.0d)
    }
    def destroy = {
    }
}
