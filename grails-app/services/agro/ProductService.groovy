package agro

import grails.gorm.services.Service
import groovy.sql.Sql

import java.sql.Connection

interface IProductService {

    Product get(Serializable id)

    List<Product> list(Map args)

    Long count()

    void delete(Serializable id)

    Product save(Product product)
}

@Service(Product)
abstract class ProductService implements IProductService {

    def filterByCategory(Long a) {
        return Product.executeQuery("from Product p where p.productCategory.id = (:id)", [id: a])
    }

    def top3RequestedProducts(Object dataSource) {
        def sql = new Sql(dataSource)
        return sql.rows("SELECT * FROM product_request_popularity where rownum < 4")
    }

    def top3SoldProducts(Object dataSource) {
        def sql = new Sql(dataSource)
        return sql.rows("SELECT * FROM product_sale_popularity where rownum < 4")
    }

    def top3PopularProducts(Object dataSource) {
        def sql = new Sql(dataSource)
        return sql.rows("SELECT * FROM product_popularity where rownum < 4")
    }
}