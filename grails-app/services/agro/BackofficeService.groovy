package agro

import grails.gorm.transactions.Transactional
import grails.web.servlet.mvc.GrailsParameterMap
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest

import javax.servlet.http.HttpServletRequest

@Transactional
class BackofficeService {

    def uploadImage(Product product, MultipartHttpServletRequest request) {
        MultipartFile bannerImageFile = request.getFile("productImage")
        if (bannerImageFile && !bannerImageFile.originalFilename.isEmpty()){
            String image = FileUtil.uploadImage(product.id, bannerImageFile, "product-image")
            if (image != ""){
                product.image = image
                product.save(flush:true)
            }
        }
    }

    def save(GrailsParameterMap params, MultipartHttpServletRequest request) {
        Product product = new Product(params as Map)
        product.productCategory = ProductCategory.get(params.categoryId as long)
        def response = AppUtil.saveResponse(false, product)
        if(product.validate()) {
            product.save(flush: true)
            if(!product.hasErrors()) {
                response.isSuccess = true
                uploadImage(product, request)
            }
        } else {
            println(product.errors)
        }
        return response
    }
}
