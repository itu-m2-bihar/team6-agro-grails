package agro

import grails.gorm.services.Service
import grails.web.servlet.mvc.GrailsParameterMap

interface IRequestService {

    Request get(Serializable id)

    List<Request> list(Map args)

    Long count()

    void delete(Serializable id)

    Request save(Request product)
}

@Service(Request)
abstract class RequestService implements IRequestService {
    def messageSource
    def save(GrailsParameterMap params, User user) {
        Request request = new Request(params)
        Product product = Product.get(params.product as Long)
        request.product = product
        request.user = user
        def response  = AppUtil.saveResponse(false, request)
//bean.save() always call validate()
        response.isSuccess = request.save(flush:true,failOnError:true)
        return response
    }

    def getOtherUsersRequest(User curUser) {
        return Request.executeQuery("from Request r where r.user.id != (:id)", [id: curUser.id])
    }
}