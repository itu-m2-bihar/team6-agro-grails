package agro

import grails.gorm.services.Service

@Service(ScratchCardCategory)
interface ScratchCardCategoryService {

    ScratchCardCategory get(Serializable id)

    List<ScratchCardCategory> list(Map args)

    Long count()

    void delete(Serializable id)

    ScratchCardCategory save(ScratchCardCategory scratchCardCategory)

}