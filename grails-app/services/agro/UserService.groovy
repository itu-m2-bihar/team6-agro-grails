package agro

import grails.gorm.services.Service
import grails.gorm.transactions.Transactional
import grails.web.servlet.mvc.GrailsParameterMap
import groovy.sql.Sql

import javax.servlet.http.HttpServletRequest

interface IUser {
    User get(Serializable id)

    List<User> list(Map args)

    Long count()

    void delete(Serializable id)

    User save(User user)
}

@Service(User)
@Transactional
abstract class UserService implements IUser {
    def uploadImage(User user, HttpServletRequest request) {
        if (request.getFile("contactImage") && !request.getFile("contactImage").filename.equals("")){
            String image = FileUtil.uploadImage(user.id, request.getFile("contactImage"), "contact-image")
            if (!image.equals("")){
                user.image = image
                user.save(flush:true)
            }
        }
    }

    def save(GrailsParameterMap params) {
        User user = new User(params)

        def response  = AppUtil.saveResponse(false, user)
        if(user.validate()) {
            user.save(flush: true)
            Role role = Role.findByAuthorityLike('ROLE_USER')
            UserRole.create(user,role)
            UserRole.withSession {
                it.flush()
                it.clear()
            }
            if(!user.hasErrors()) {
                response.isSuccess = true
//                uploadImage(user, request)
            } else {
                response.isSuccess = false
            }
        } else {
            response.isSuccess = false
            if(user.password == null || user.password.isEmpty()) {
                response.model = ["Mot de passe obligatoire"];
            } else {
                response.model = ["Nom d'utilisateur indisponile"];
            }
        }
        return response
    }

    def getMostRecentCart(User user) {
        return Cart.executeQuery("from Cart cart where cart.user.id = (:id) and cart.cartState = (:state) ORDER BY cart.id", [max: 1, id: user.id, state: CartState.CREATED.ordinal()])
    }
    def getPaidCart(User user) {
        return  Cart.executeQuery("from Cart cart where cart.user.id = (:id) and cart.cartState = (:state)", [id: user.id,state: CartState.PAID.ordinal()])
    }

    def getValidSales(User user) {
        return Sale.executeQuery("from Sale sale where sale.user.id = (:id) and sale.saleState >= (:state) ORDER BY sale.id", [id: user.id, state: SaleState.CREATED.ordinal()])
    }

    def top3EarningsUsers(Object dataSource) {
        def sql = new Sql(dataSource)
        return sql.rows("SELECT * FROM user_total_earnings where rownum < 4")
    }

    def top3PurchasesUsers(Object dataSource) {
        def sql = new Sql(dataSource)
        return sql.rows("SELECT * FROM user_total_purchases where rownum < 4")
    }
}