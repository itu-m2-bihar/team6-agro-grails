package agro

import grails.gorm.services.Service

interface IProposalService {

    Proposal get(Serializable id)

    List<Proposal> list(Map args)

    Long count()

    void delete(Serializable id)

    Proposal save(Product product)
}

