package agro

import grails.gorm.services.Service

@Service(ScratchCard)
interface ScratchCardService {

    ScratchCard get(Serializable id)

    List<ScratchCard> list(Map args)

    Long count()

    void delete(Serializable id)

    ScratchCard save(ScratchCard scratchCard)

}