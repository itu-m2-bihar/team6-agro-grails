package agro

import grails.gorm.services.Service
import grails.web.servlet.mvc.GrailsParameterMap

interface ISaleService {
    Sale get(Serializable id)

    List<Sale> list(Map args)

    Long count()

    void delete(Serializable id)

    Sale save(Sale sale)
}

@Service(Sale)
abstract class SaleService implements ISaleService {
    def filterByProductNames(List<String> productNames, Integer page, Integer max) {
        return Sale.executeQuery("from Sale s where s.product.name IN (:list)", [list: productNames, offset: max*(page-1), max: max])
    }

    def getValidSalesByProducts(List<String> productNames, Integer page, Integer max,User user){
        Long idU = 0;
        if (user != null){
            idU = user.id;
        }
        return Sale.executeQuery("from Sale s where s.saleState=(:state) and s.product.name IN (:list) and s.user.id != (:id)", [list: productNames, offset: max*(page-1), max: max, state: SaleState.CREATED.ordinal(), id:idU])
    }

    def getValidSales(Integer page, Integer max,User user){
        Long idU = 0;
        if (user != null){
            idU = user.id;
        }
        return Sale.executeQuery("from Sale s where s.saleState=(:state) and s.user.id != (:id)", [offset: max*(page-1), max: max, state: SaleState.CREATED.ordinal(), id:idU])

    }

    def filterByIds(Long[] ids) {
        return Sale.executeQuery("from Sale s where s.id IN (:list)", [list: ids])
    }
    def saveSell(GrailsParameterMap params, User user) {
        Sale sale = new Sale(params)

        Product product = Product.get(params.product as Long)
        sale.product = product
        sale.user = user
        sale.saleState = 1

        def response  = AppUtil.saveResponse(false, sale)
        if(sale.validate()) {
            sale.save(flush:true,failOnError:true)
            if(!sale.hasErrors()) {
                response.isSuccess = true
            } else {
                response.isSuccess = false
            }
        } else {
            response.isSuccess = false
        }
        return response
    }
}