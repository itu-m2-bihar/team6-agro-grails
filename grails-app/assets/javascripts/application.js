// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better
// to create separate JavaScript files as needed.
//
//= require jquery-2.2.0.min
//= require bootstrap
//= require_tree .
//= require_self

if (typeof jQuery !== 'undefined') {
    (function($) {
        $(document).ajaxStart(function() {
            $('#spinner').fadeIn();
        }).ajaxStop(function() {
            $('#spinner').fadeOut();
        });
    })(jQuery);
}

function setCookie(key, value, expiry) {
    var expires = new Date();
    expires.setTime(expires.getTime() + (expiry * 24 * 60 * 60 * 1000));
    // document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
    document.cookie = key + '=' + value + ';path=/' + ';expires=' + expires.toUTCString();
}

function getCookie(key) {
    var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
    return keyValue ? keyValue[2] : null;
}

function eraseCookie(key) {
    var keyValue = getCookie(key);
    setCookie(key, keyValue, '-1');
}

function addToCart(saleId, addBtnId, removeBtnId) {
    $("#"+addBtnId).hide();
    $("#"+removeBtnId).show();
    let cookie = getCookie('cart');
    var items = cookie ? cookie.split(/_/) : new Array();
    items.push(saleId);
    setCookie('cart',items.join('_'), '30');
}

function removeFromCart(saleId, addBtnId, removeBtnId) {
    $("#"+removeBtnId).hide();
    $("#"+addBtnId).show();
    let cookie = getCookie('cart');
    var items = cookie ? cookie.split(/_/) : new Array();
    items.push(saleId);
    setCookie('cart',items.filter(i => i != saleId).join('_'), '30');
}
function updateProduct(url, containerId){
    let categoryId = $('#category').find(":selected").val();
    if(categoryId !== '') {
        $.ajax({
            url:url,
            data: {
                category: categoryId
            },
            type: 'get'
        }).done(
            function ( data ) {
                document.getElementById(containerId).innerHTML = data;
            }
        );
    } else {
        $('#product').children().remove();
    }
}
function propose(event, requestId, that, url) {
    event.preventDefault();
    let data = {
        requestId,
        price: that.price.value
    }

    $.ajax({
        url:url,
        data,
        type: 'post'
    }).done(
        function ( data ) {
            $("#request-card-"+requestId).html(data);
        }
    );
}
function cancelProposal(event, proposalId, requestId, that, url) {
    event.preventDefault();
    let data = {
        proposalId,
        requestId
    }

    $.ajax({
        url:url,
        data,
        type: 'post'
    }).done(
        function ( data ) {
            $("#request-card-"+requestId).html(data);
        }
    );
}