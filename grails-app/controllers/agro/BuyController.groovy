package agro

import agro.dto.CheckableString
import grails.plugin.cookie.CookieService
import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured

@Secured("IS_AUTHENTICATED_ANONYMOUSLY")
class BuyController {
    CookieService cookieService
    SaleService saleService
    ProductService productService
    UserService userService
    SpringSecurityService springSecurityService


    def index() {
        User curUser = springSecurityService.currentUser as User
        List<CheckableString> checkableProducts = []
        List<String> productParams = []
        def productsNames = productService.list()

        if (params['produits[]'] != null) {
            if (params['produits[]'] instanceof String) {
                productParams.push(params['produits[]'] as String)
            } else {
                def temp = params['produits[]'] as List<String>
                temp.forEach() {
                    productParams.push(it)
                }
            }
        }

        productsNames.forEach() {
            def temp = new CheckableString(string: it.name, checked: false);
            if (productParams.contains(it.name)) {
                temp.checked = true;
            }
            checkableProducts.push(temp);
        }

        params.max = Math.min(params.max as Integer ?: 10, 100)
        params.remove('produits[]')

        def a = saleService.getValidSales(params.page == null ? 1 : params.page as Integer, params.max as Integer,curUser);


        if (!productParams.isEmpty()) {
            a = saleService.getValidSalesByProducts(productParams, params.page as Integer, params.max as Integer,curUser)
        }

        String cookie = cookieService.getCookie('cart')
        List<String> cart = null
        if (cookie != null && !cookie.isEmpty()) {
            cart = cookie.split(/_/)
        }


        Cart mostRecentCart = null
        if (curUser != null) {
            def queriesRes = userService.getMostRecentCart(curUser)
            if (!queriesRes.isEmpty()) {
                mostRecentCart = queriesRes.first() as Cart
                if (cart != null) { // if existent cookie
                    if (mostRecentCart.sales == null) {
                        mostRecentCart.sales = []
                    }
                    for (i in 0..<cart.size()) {
                        def sale = Sale.get(cart[i] as long)
                        if (!mostRecentCart.sales.contains(sale)) {
                            mostRecentCart.sales.add(sale)
                        }
                    }
                }
                cart = []
                for (i in 0..<mostRecentCart.sales.size()) {
                    def sale = mostRecentCart.sales[i]
                    if (sale.saleState != SaleState.CREATED.ordinal()) { // if sale deactivated or sold
                        mostRecentCart.removeFromSales(sale)
                    } else {
                        cart.add(mostRecentCart.sales[i].id as String)
                    }
                }
                cookieService.setCookie('cart', cart.join('_'), 1, '/')
                mostRecentCart.save(flush: true, failOnError: true)
            } else {
                Cart newCart = new Cart();
                newCart.sales = []
                curUser.addToCarts(newCart);
                if (cart != null) { // if existent cookie
                    for (i in 0..<cart.size()) {
                        def sale = Sale.get(cart[i] as long)
                        if (sale.saleState != SaleState.CREATED.ordinal()) {
                            newCart.sales.add(sale)
                        }
                    }
                }
                cart = []
                for (i in 0..<newCart.sales.size()) {
                    cart.add(newCart.sales[i].id as String)
                }
                newCart.save(flush: true, failOnError: true)
                cookieService.setCookie('cart', cart.join('_'), 1, '/')
            }
        }

        respond a, model: [products: checkableProducts, sales: a, page: params.page as Integer, cart: cart]
    }
}
