package agro

import grails.plugin.springsecurity.annotation.Secured
import org.springframework.web.multipart.MultipartHttpServletRequest

@Secured(['ROLE_ADMIN'])
class BackofficeController {
    ProductCategoryService productCategoryService
    BackofficeService backofficeService
    ProductService productService
    UserService userService
    ScratchCardCategoryService scratchCardCategoryService
    ScratchCardService scratchCardService
    def dataSource

    def index() {}

    def addProduct() {
        def categories = productCategoryService.list(params)
        def products = productService.list()
        respond 1, model: [categories: categories, products: products]
    }

    def productStats() {
        def top3RequestedProducts = productService.top3RequestedProducts(dataSource)
        def top3SoldProducts = productService.top3SoldProducts(dataSource)
        def top3PopularProducts = productService.top3PopularProducts(dataSource)
        respond 1, model: [top3RequestedProducts: top3RequestedProducts, top3SoldProducts: top3SoldProducts, top3PopularProducts: top3PopularProducts]
    }

    def userStats() {
        def top3EarningsUsers = userService.top3EarningsUsers(dataSource)
        def top3PurchasesUsers = userService.top3PurchasesUsers(dataSource)
        respond 1, model: [top3EarningsUsers: top3EarningsUsers, top3PurchasesUsers: top3PurchasesUsers]
    }

    def editUser() {
        def user = User.get(params.id as long)
        respond 1, model: [user: user, roles: Role.list()]
    }

    def saveEdit() {
        List<Long> rolesParams = []
        if (params['roles[]'] != null) {
            if (params['roles[]'] instanceof String) {
                rolesParams.push(params['roles[]'] as Long)
            } else {
                def temp = params['roles[]'] as List<String>
                temp.forEach() {
                    rolesParams.push(it as Long)
                }
            }
        }
        def roles = Role.getAll(rolesParams)

        def user = User.get(params.userId as long)

        if (params.submit == "ban") {
            user.accountLocked = true
            user.save(flush: true, failOnError: true)
        } else {
            roles.each {
                UserRole.findOrSaveWhere([user: user, role: it])
            }
            UserRole.withSession {
                it.flush()
                it.clear()
            }
        }

        redirect(controller: "backoffice", action: "userModeration")
    }

    def userModeration() {
        def page = 1
        params.max = Math.min(params.max as Integer ?: 10, 100)
        if (params.page) {
            page = params.page as int
            params.offset = (params.max as int) * ((params.page as int) - 1)
        }
        def users = userService.list(params)
        respond 1, model: [users: users, page: page]
    }

    def save() {
        def response = backofficeService.save(params, request as MultipartHttpServletRequest)
        if (response.isSuccess) {
            flash.success = "Produit ajouté"
            redirect(controller: "backoffice", action: "addProduct")
        } else {
            flash.message = AppUtil.infoMessage("unable to save")
            redirect(controller: "backoffice", action: "addProduct")
        }
    }

    def generateScratchCard() {
        def categories = scratchCardCategoryService.list()

        if(params.categoryId) {
            def category = ScratchCardCategory.get(params.categoryId as long)
            for (i in 0..<100) {
                new ScratchCard(category: category).save(flush: true)
            }
        }
        def cards = scratchCardService.list()
        respond 1, model: [categories: categories, cards: cards]
    }
}
