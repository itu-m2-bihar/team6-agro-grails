package agro

import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_USER'])
class CreateRequestController {
    ProductService productService
    ProductCategoryService productCategoryService
    RequestService requestService
    SpringSecurityService springSecurityService
    def products = null


    def index = {
        def categories = productCategoryService.list()
        respond 1, model: [categories: categories,products: products]
    }
    def findProductByCategory(params) {
        products = productService.filterByCategory(params.category as Long);
        render(template: '/templates/productSelect', model:  [product: products])
    }
    def publy() {
        def response = requestService.save(params, springSecurityService.currentUser)
        if(response.isSuccess) {
            flash.success = "Demande publié"
            redirect(controller: "myRequests")
        } else {
//            def errors = response.model as String[];
//            flash.errors = errors
//            respond 1, view: 'index'
            print("error")
            redirect(controller: "createRequest",model:[request:response.request])
        }
    }
}
