package agro

import agro.dto.CheckableString
import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured

@Secured("IS_AUTHENTICATED_ANONYMOUSLY")
class HomeController {
    ProductService productService
    SpringSecurityService springSecurityService
    
    def index() {
        List<CheckableString> checkableProducts = []
        List<String> productParams = []
        def productsNames = productService.list()

        if (params['produits[]'] != null) {
            if (params['produits[]'] instanceof String) {
                productParams.push(params['produits[]'] as String)
            } else {
                def temp = params['produits[]'] as List<String>
                temp.forEach() {
                    productParams.push(it)
                }
            }
        }

        productsNames.forEach() {
            def temp = new CheckableString(string: it.name, checked: false);
            if (productParams.contains(it.name)) {
                temp.checked = true;
            }
            checkableProducts.push(temp);
        }

        respond 1, model: [products: checkableProducts]
    }
}
