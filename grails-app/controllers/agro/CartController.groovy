package agro

import grails.plugin.cookie.CookieService
import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured

@Secured("IS_AUTHENTICATED_ANONYMOUSLY")
class CartController {
    CookieService cookieService
    SpringSecurityService springSecurityService
    UserService userService
    SaleService saleService

    def index() {
        String cookie = cookieService.getCookie('cart')
        List<String> cart = null
        double total = 0
        List<Sale> sales = [];

        if (cookie != null && !cookie.isEmpty()) {
            cart = cookie.split(/_/)
            Long[] ids = cart*.toLong()
            sales = saleService.filterByIds(ids)
        }

        User curUser = springSecurityService.currentUser as User
        Cart mostRecentCart = null
        if (curUser != null) {
            def queriesRes = userService.getMostRecentCart(curUser)
            if (!queriesRes.isEmpty()) {
                mostRecentCart = queriesRes.first() as Cart
                println("bot:"+mostRecentCart.id)
                if (cart != null) {
                    if (mostRecentCart.sales == null) {
                        mostRecentCart.sales = []
                    }
                    for (i in 0..<cart.size()) {
                        def sale = Sale.get(cart[i] as long)
                        if (!mostRecentCart.sales.contains(sale)) {
                            mostRecentCart.sales.add(sale)
                        }
                    }
                }
                cart = []
                for (i in 0..<mostRecentCart.sales.size()) {
                    def sale = mostRecentCart.sales[i]
                    if (sale.saleState != SaleState.CREATED.ordinal()) { // if sale deactivated or sold
                        mostRecentCart.removeFromSales(sale)
                    } else {
                        sales.add(mostRecentCart.sales[i])
                    }
                }
                cookieService.setCookie('cart', cart.join('_'), 1, '/')
                mostRecentCart.save(flush: true, failOnError: true)
            } else {
                Cart newCart = new Cart();
                newCart.sales = []
                curUser.addToCarts(newCart);
                if (cart != null) { // if existent cookie
                    for (i in 0..<cart.size()) {
                        def sale = Sale.get(cart[i] as long)
                        if(sale.saleState != SaleState.CREATED.ordinal()) {
                            newCart.sales.add(sale)
                        }
                    }
                }
                cart = []
                for (i in 0..<newCart.sales.size()) {
                    sales.add(newCart.sales[i])
                }
                newCart.save(flush: true, failOnError: true)
                mostRecentCart = newCart
                cookieService.setCookie('cart', cart.join('_'), 1, '/')
            }
        }

        if (!sales.isEmpty()) {
            total = sales.sum { it.price * it.quantity }
        }

        respond 1, model: [sales: sales, cart: mostRecentCart, total: total]
    }

    def confirmCart() {
        Cart cart = Cart.get(params.cartId as long)
        double total = cart.sales.sum {
            it.price*it.quantity
        } as double

        User curUser = springSecurityService.currentUser as User
        if(curUser.virtualMoney < total) {
            flash.message = "Token insuffisant"
            redirect(controller: "cart")
            return
        }

        MvntSale mvntSaleCurUser = new MvntSale()
        mvntSaleCurUser.user = curUser
        mvntSaleCurUser.mvntType = -1
        mvntSaleCurUser.cart = cart
        mvntSaleCurUser.price = total
        curUser.virtualMoney -= total

        mvntSaleCurUser.save(flush: true, failOnError: true)
        curUser.save(flush: true, failOnError: true)

        cart.sales.forEach() { sale ->
            def mvntSale = new MvntSale()
            mvntSale.user = sale.user
            mvntSale.mvntType = 1
            mvntSale.cart = cart
            mvntSale.price = sale.price
            sale.user.virtualMoney += (sale.price*sale.quantity)
            sale.saleState = SaleState.PAID.ordinal()
            mvntSale.save(flush: true, failOnError: true)
            sale.save(flush: true, failOnError: true)
            sale.user.save(flush: true, failOnError: true)
        }

        cart.cartState = CartState.PAID.ordinal()
        cart.save(flush: true, failOnError: true)

        flash.success = "Achat effectué!"
        redirect(controller: "cart")
    }
}
