package agro

import grails.plugin.springsecurity.annotation.Secured

@Secured("IS_AUTHENTICATED_ANONYMOUSLY")
class PredictController {
    ProductCategoryService productCategoryService
    ProductService productService
    def products = null;

    def index() {
        def categories = productCategoryService.list()
        respond 1, model: [categories: categories]
    }

    def getCategories(){
        def categories = productCategoryService.list()
        render  (contentType: "application/json"){
            categList (categories) { ProductCategory categ ->
                id categ.id
                name categ.name
            }
        }
    }

    def findProductByCategory(params) {
        products = productService.filterByCategory(params.category as Long);
        render(template: '/templates/productSelect', model:  [product: products])
    }
}
