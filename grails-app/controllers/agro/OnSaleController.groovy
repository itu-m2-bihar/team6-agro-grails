package agro

import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_USER'])
class OnSaleController {
    SpringSecurityService springSecurityService
    UserService userService

    def index() {
        User user = springSecurityService.currentUser
        respond 1, model: [sales: userService.getValidSales(user)]
    }
    def remove() {
        def sale = Sale.get(params.id as Long)
        sale.delete(flush: true)
        redirect(controller: "onSale")
    }
}
