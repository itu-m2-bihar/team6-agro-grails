package agro

import grails.plugin.springsecurity.annotation.Secured

@Secured("IS_AUTHENTICATED_ANONYMOUSLY")
class RestController {
    def productService
    def userService

    def getProductById(Integer id) {
        respond productService.get(id), model: []
    }

    def getUserById(Integer id) {
        respond userService.get(id), model: []
    }
}
