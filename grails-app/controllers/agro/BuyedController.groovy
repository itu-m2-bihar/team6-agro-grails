package agro

import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_USER'])
class BuyedController {
    SpringSecurityService springSecurityService
    UserService userService
    SaleService saleService

    def index() {
        List<String> cart = null
        double total = 0
        List<Sale> sales = []
        User curUser = springSecurityService.currentUser as User
        if (curUser != null) {
            def carts = userService.getPaidCart(curUser)
            respond 1, model: [panier: carts]
        }
    }
}
