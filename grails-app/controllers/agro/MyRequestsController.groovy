package agro

import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_USER'])
class MyRequestsController {
    SpringSecurityService springSecurityService
    ProductCategoryService productCategoryService

    def index() {
        if(params.id != null) {
            def request = Request.get(params.id as Long)
            def categories = productCategoryService.list()
            respond 1, model: [request: request, categories: categories], view: 'edit'
        } else {
            User user = springSecurityService.currentUser
            respond 1, model: [requests: user.requests]
        }
    }
    def remove() {
        def request = Request.get(params.id as Long)
        request.delete(flush: true)
        redirect(controller: "myRequests")
    }
    def edit() {
        def request = Request.get(params.id as Long)
        request.delete(flush: true)
        redirect(controller: "myRequests")
    }

    def saveEdit() {
        Request req = Request.get(params.id as Long)
        req.quantity = params.quantity as double
        Product product = Product.get(params.product as Long)
        req.product = product

        req.save(flush: true)
        redirect(controller: "myRequests")
    }

    def acceptProposal() {
        Request req = Request.get(params.requestId as long)
        Proposal proposal = Proposal.get(params.proposalId as long)
        User curUser = springSecurityService.currentUser
        User proposer = proposal.proposer

        if(curUser.virtualMoney >= proposal.price) {
            MvntRequest mvntRequestCurUser = new MvntRequest()
            mvntRequestCurUser.user = curUser
            mvntRequestCurUser.mvntType = -1
            mvntRequestCurUser.request = req
            mvntRequestCurUser.price = proposal.price

            MvntRequest mvntRequestProposer = new MvntRequest()
            mvntRequestProposer.user = proposer
            mvntRequestProposer.mvntType = 1
            mvntRequestProposer.request = req
            mvntRequestProposer.price = proposal.price

            mvntRequestCurUser.save(flush:true, failOnError:true)
            mvntRequestProposer.save(flush:true, failOnError:true)

            curUser.virtualMoney -= proposal.price
            proposer.virtualMoney += proposal.price
            req.proposals.forEach() {
                it.proposalState = ProposalState.CLOSED.ordinal()
            }
            req.requestState = ProposalState.PAID.ordinal()
            proposal.proposalState = ProposalState.PAID.ordinal()

            req.save(flush: true)
            proposal.save(flush: true)
            curUser.save(flush: true)
            proposer.save(flush: true)
        } else {
            flash.message = "Token insuffisant";
        }
        redirect(controller: "myRequests")
    }
}
