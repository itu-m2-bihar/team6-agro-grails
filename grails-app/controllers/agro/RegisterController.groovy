package agro

import grails.plugin.springsecurity.annotation.Secured

import static org.springframework.http.HttpStatus.NOT_FOUND

@Secured("IS_AUTHENTICATED_ANONYMOUSLY")
class RegisterController {
    UserService userService

    def index(User user) {
        if (user != null) {
            try {
                def response = userService.save(params)
                if (response.isSuccess) {
                    flash.success = "Connectez-vous pour continuer."
                    redirect(controller: "login", action: "auth")
                } else {
                    def errors = response.model as String[];
                    flash.errors = errors
                    respond 1, view: 'index'
                }
            } catch (Exception e) {
                respond user.errors, view:'index'
            }
        }
    }


    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
