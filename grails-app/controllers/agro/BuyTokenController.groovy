package agro

import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_USER'])
class BuyTokenController {
    SpringSecurityService springSecurityService

    def index() {
        if(params.code) {
            def curUser = springSecurityService.currentUser as User
            def code = params.code as String
            def card = ScratchCard.get(code)
            if(card) {
                MvntScratchCard mvntScratchCard = new MvntScratchCard(user: curUser, scratchCard: card)
                mvntScratchCard.save(flush: true)
                curUser.virtualMoney += card.category.amount
                curUser.save(flush: true)
                flash.success = "Token crédité."
            } else {
                flash.message = "Code invalide."
            }
        }
        respond 1, model: []
    }
}
