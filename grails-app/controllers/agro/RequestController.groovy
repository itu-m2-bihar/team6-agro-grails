package agro

import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured

@Secured("IS_AUTHENTICATED_ANONYMOUSLY")
class RequestController {
    RequestService requestService
    SpringSecurityService springSecurityService

    def index() {
        if (springSecurityService.currentUser != null) {
            User curUser = springSecurityService.currentUser
            respond 1, model: [requests: requestService.getOtherUsersRequest(springSecurityService.currentUser), curUser: curUser]
        } else {
            respond 1, model: [requests: requestService.list(params)]
        }
    }

    def propose() {
        Proposal proposal = new Proposal(params)
        def req = Request.get(params.requestId as long)
        proposal.request = req
        req.addToProposals(proposal)

        User curUser = springSecurityService.currentUser
        curUser.addToProposals(proposal)

        proposal.proposer = curUser
        proposal.save(flush: true)
        req.save(flush: true)
        curUser.save(flush: true)
        render(template: '/templates/requestCard', model: [req: req, curUser: curUser])
    }

    def cancelProposal() {
        Proposal proposal = Proposal.get(params.proposalId as long)

        def req = Request.get(params.requestId as long)
        req.removeFromProposals(proposal)

        User curUser = springSecurityService.currentUser
        curUser.removeFromProposals(proposal)

        proposal.delete(flush: true)
        curUser.save(flush: true)
        req.save(flush: true)
        render(template: '/templates/requestCard', model: [req: req, curUser: curUser])
    }
}
