package agro

import javax.persistence.GeneratedValue

class Product {
    @GeneratedValue
    Long id
    String name
    ProductCategory productCategory
    String image

    static constraints = {
        id generator: 'sequence', params:[sequence:'PRODUCT_SEQ'], column: 'product_id'
        name column: 'product_name', nullable: false, blank: false
        image nullable: true
    }
}
