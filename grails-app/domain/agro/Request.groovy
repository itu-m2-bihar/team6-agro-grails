package agro

class Request {
    Long id
    Product product
    double quantity
    User user
    Date dateCreated
    Date lastUpdated
    int requestState = RequestState.CREATED.ordinal()

    static hasMany = [proposals: Proposal]

    static constraints = {
        id generator: 'sequence', params:[sequence:'REQUEST_SEQ'], column: 'request_id'
        quantity nullable: false
        requestState nullable: false, defaultValue: RequestState.DEACTIVATED.ordinal()
    }

    Proposal findProposal(Set<Proposal> props) {
        def index = -1
        for (i in 0..<props.size()) {
            index = proposals.findIndexOf {
                props[i] == it
            }
            if(index != -1)
                return props[i]
        }
        return null
    }
}
