package agro

class ScratchCard {
    String id = new RandomString(12).nextString()
    ScratchCardCategory category
    Date dateCreated

    static mapping = {
        id generator: 'assigned'
    }

    static constraints = {
    }
}
