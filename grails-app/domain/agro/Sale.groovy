package agro

class Sale {
    Long id
    Product product
    int saleState = SaleState.CREATED.ordinal()
    double price
    double quantity
    Date dateCreated
    Date lastUpdated

    static belongsTo = [user: User]

    static constraints = {
        id generator: 'sequence', params:[sequence:'SALE_SEQ'], column: 'sale_id'
        price nullable: false
        saleState defaultValue: SaleState.DEACTIVATED.ordinal(), nullable: false
        quantity defaultValue: 0, nullable: false
    }

}
