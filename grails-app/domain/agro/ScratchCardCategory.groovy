package agro

class ScratchCardCategory {
    double amount

    static constraints = {
        id generator: 'sequence', params:[sequence:'SCRATCH_CARD_CATEGORY_SEQ']
        amount unique: true
    }
}
