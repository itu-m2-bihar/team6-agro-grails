package agro

class ProductCategory {
    Long id
    String name

    static constraints = {
        id generator: 'sequence', params:[sequence:'PRODUCT_CATEGORY_SEQ'], column: 'category_id'
        name column: 'category_name', nullable: false, blank: false
    }
}
