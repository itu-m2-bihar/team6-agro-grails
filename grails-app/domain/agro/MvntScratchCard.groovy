package agro

class MvntScratchCard {
    ScratchCard scratchCard
    User user
    Date dateCreated

    static constraints = {
        id generator: 'sequence', params:[sequence:'MVNT_SCRATCH_CARD_SEQ']
    }
}
