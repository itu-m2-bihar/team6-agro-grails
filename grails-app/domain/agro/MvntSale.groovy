package agro

class MvntSale {
    Long id
    int mvntType
    Cart cart
    User user
    double price
    Date dateCreated

    static constraints = {
        id generator: 'sequence', params:[sequence:'MVNT_SALE_SEQ'], column: 'mvnt_sale_id'
    }
}
