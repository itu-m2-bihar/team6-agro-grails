package agro

class Proposal {
    double price
    static belongsTo = [request: Request, proposer: User]
    static hasOne = [proposer: User]
    int proposalState = ProposalState.CREATED.ordinal()
    Date dateCreated
    Date lastUpdated

    static constraints = {
        id generator: 'sequence', params:[sequence:'PROPOSAL_SEQ']
        proposalState nullable: false, defaultValue: ProposalState.DEACTIVATED.ordinal()
    }
}
