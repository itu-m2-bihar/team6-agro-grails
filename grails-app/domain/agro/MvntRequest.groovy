package agro

class MvntRequest {
    Long id
    int mvntType
    Request request
    User user
    double price
    Date dateCreated

    static constraints = {
        id generator: 'sequence', params:[sequence:'MVNT_REQUEST_SEQ'], column: 'mvnt_request_id'
    }
}
