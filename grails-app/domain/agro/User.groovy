package agro

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import grails.compiler.GrailsCompileStatic

@GrailsCompileStatic
@EqualsAndHashCode(includes='username')
@ToString(includes='username', includeNames=true, includePackage=false)
class User implements Serializable {

    private static final long serialVersionUID = 1
    String username
    String password
    double virtualMoney = 10000

    boolean enabled = true
    boolean accountExpired
    boolean accountLocked
    boolean passwordExpired

    static hasMany = [
            requests: Request,
            mySales: Sale,
            carts: Cart,
            proposals: Proposal
    ]

    Set<Role> getAuthorities() {
        (UserRole.findAllByUser(this) as List<UserRole>)*.role as Set<Role>
    }

    static constraints = {
        id generator: 'sequence', params:[sequence:'USER_SEQ']
        password nullable: false, blank: false, password: true
        username nullable: false, blank: false, unique: true
        virtualMoney nullable: false, defaultValue: 10000
    }

    static mapping = {
        table 'USER_AGRO'
	    password column: '`password`'
    }
}
