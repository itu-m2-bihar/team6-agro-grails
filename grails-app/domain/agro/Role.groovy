package agro

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import grails.compiler.GrailsCompileStatic

@GrailsCompileStatic
@EqualsAndHashCode(includes='authority')
@ToString(includes='authority', includeNames=true, includePackage=false)
class Role implements Serializable {

	private static final long serialVersionUID = 1
	Long id
	String authority

	static constraints = {
		id generator: 'sequence', params:[sequence:'ROLE_SEQ']
		authority nullable: false, blank: false, unique: true
	}

	static mapping = {
		cache true
	}
}
