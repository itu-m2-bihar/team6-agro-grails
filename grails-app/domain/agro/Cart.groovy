package agro

class Cart {
    static belongsTo = [user: User]
    int cartState = CartState.CREATED.ordinal()
    Date dateCreated
    Date lastUpdated

    static hasMany = [sales: Sale]

    static constraints = {
        id generator: 'sequence', params:[sequence:'CART_SEQ']
        cartState nullable: false, defaultValue: CartState.DEACTIVATED.ordinal()
    }

    static mapping = {
        sales joinTable: [name: 'cart_sale', key: ['cart_id', 'sale_id'], key: 'cart_id', column: 'sale_id']
    }
}
