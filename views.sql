CREATE VIEW product_sale_popularity AS
SELECT COUNT(s.product_id) sale_popularity, p.product_id pid, p.product_name pname, p.image
FROM mvnt_sale ms JOIN cart_sale cs ON ms.cart_id=cs.cart_id
JOIN sale s ON s.sale_id=cs.sale_id RIGHT JOIN product p ON p.product_id=s.product_id
GROUP BY p.PRODUCT_ID , p.PRODUCT_NAME, p.IMAGE  ORDER BY sale_popularity DESC


CREATE VIEW product_request_popularity AS
SELECT COUNT(r.product_id) request_popularity, p.product_id pid, p.product_name pname, p.image
from mvnt_request mr JOIN request r ON mr.request_id=r.request_id
RIGHT JOIN product p ON p.product_id = r.product_id
GROUP BY p.product_name, p.product_id, p.image ORDER BY request_popularity DESC




CREATE VIEW product_popularity AS
SELECT pr.pid, pr.pname, (sale_popularity + request_popularity) popularity, pr.image
FROM product_request_popularity pr JOIN product_sale_popularity ps ON pr.pid=ps.pid ORDER BY popularity DESC

--gains
CREATE VIEW requests_earnings AS
SELECT * FROM mvnt_request WHERE mvnt_type=1

CREATE VIEW sales_earnings AS
SELECT * FROM mvnt_sale WHERE mvnt_type=1

CREATE VIEW user_requests_earnings AS
SELECT COALESCE(SUM(price),0) earnings, u.id usid, u.username usname
FROM user_agro u lEFT JOIN requests_earnings re ON u.id=re.user_id
GROUP BY u.id , u.username ORDER BY earnings DESC

CREATE VIEW user_sales_earnings AS
SELECT COALESCE(SUM(price),0) earnings, u.id usid, u.username usname
FROM user_agro u LEFT JOIN sales_earnings se ON u.id=se.user_id
GROUP BY u.id, u.username ORDER BY earnings DESC

CREATE VIEW user_total_earnings AS
SELECT (usea.earnings + urea.earnings) total_earnings, urea.usid, urea.usname
FROM user_requests_earnings urea JOIN user_sales_earnings usea ON usea.usid=urea.usid ORDER BY total_earnings DESC

--depenses
CREATE VIEW requests_purchases AS
SELECT * FROM mvnt_request WHERE mvnt_type=-1

CREATE VIEW sales_purchases AS
SELECT * FROM mvnt_sale WHERE mvnt_type=-1

CREATE VIEW user_requests_purchases AS
SELECT COALESCE(SUM(price),0) purchases, u.id usid, u.username usname
FROM user_agro u lEFT JOIN requests_purchases rp ON u.id=rp.user_id
GROUP BY u.id, u.username ORDER BY purchases DESC

CREATE VIEW user_sales_purchases AS
SELECT COALESCE(SUM(price),0) purchases, u.id usid, u.username usname
FROM user_agro u LEFT JOIN sales_purchases sp ON u.id=sp.user_id
GROUP BY u.id, u.username ORDER BY purchases DESC

CREATE VIEW user_total_purchases AS
SELECT (uspa.purchases + urpa.purchases) total_purchases, urpa.usid, urpa.usname
FROM user_requests_purchases urpa JOIN user_sales_purchases uspa ON uspa.usid=urpa.usid ORDER BY total_purchases DESC