package agro

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class SaleServiceSpec extends Specification {

    SaleService saleService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new Sale(...).save(flush: true, failOnError: true)
        //new Sale(...).save(flush: true, failOnError: true)
        //Sale sale = new Sale(...).save(flush: true, failOnError: true)
        //new Sale(...).save(flush: true, failOnError: true)
        //new Sale(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //sale.id
    }

    void "test get"() {
        setupData()

        expect:
        saleService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<Sale> saleList = saleService.list(max: 2, offset: 2)

        then:
        saleList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        saleService.count() == 5
    }

    void "test delete"() {
        Long saleId = setupData()

        expect:
        saleService.count() == 5

        when:
        saleService.delete(saleId)
        sessionFactory.currentSession.flush()

        then:
        saleService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        Sale sale = new Sale()
        saleService.save(sale)

        then:
        sale.id != null
    }
}
