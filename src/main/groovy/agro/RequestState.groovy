package agro

enum RequestState {
    DEACTIVATED,
    CREATED,
    CLOSED,
    PAID
}