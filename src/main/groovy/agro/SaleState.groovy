package agro

enum SaleState {
    DEACTIVATED,
    CREATED,
    CLOSED,
    PAID
}