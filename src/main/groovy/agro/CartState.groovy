package agro

enum    CartState {
    DEACTIVATED,
    CREATED,
    PAID
}