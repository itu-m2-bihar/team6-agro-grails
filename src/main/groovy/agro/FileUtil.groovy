package agro

import grails.web.context.ServletContextHolder
import org.springframework.web.multipart.MultipartFile

class FileUtil {

    static String getRootPath(){
        return ServletContextHolder.servletContext?.getRealPath("")
    }


    static File makeDirectory(String path){
        File file = new File(path)
        if (!file.exists()){
            file.mkdirs()
        }
        return file
    }

    static String uploadImage(Long id, MultipartFile multipartFile, String folderName){
        if (id && multipartFile){
            String imagePath = "${getRootPath()}$folderName/"
            makeDirectory(imagePath)
            multipartFile.transferTo(new File(imagePath, id + "-" + multipartFile.originalFilename))
            return multipartFile.originalFilename
        }
        return ""
    }
}
