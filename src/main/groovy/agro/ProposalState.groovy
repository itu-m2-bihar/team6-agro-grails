package agro

enum ProposalState {
    DEACTIVATED,
    CREATED,
    CLOSED,
    PAID
}